<?php
session_start();
include_once('../../vendor/autoload.php');
use App\Bitm\SEIP137157\Hobby\Hobby;
use App\Bitm\SEIP137157\Utility\Utility;
use App\Bitm\SEIP137157\Message\Message;



?>

<!DOCTYPE html>

<head>
    <title>ATOMIC PROJECTS - INDEX PAGE</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../resource/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../resource/bootstrap/js/jquery.min.js"></script>
</head>
<body>
<div class="container">
    <h2>ATOMIC PROJECT - INDEX</h2>


    <table>
        <tr>
            <td height="100">


                <div id="AtomicProjectListMenu">
                    <button type="button" onclick="window.location.href='BookTitle/index.php'" class=" btn-success btn-lg">Book Title</button>
                    <button type="button" onclick="window.location.href='Birthday/index.php'" class=" btn-primary btn-lg">Birthday</button>

                    <button type="button" onclick="window.location.href='City/index.php'" class=" btn-danger btn-lg">City</button>
                    <button type="button" onclick="window.location.href='EmailSubscription/index.php'" class=" btn-primary btn-lg">Email Subscription</button>
                    <button type="button" onclick="window.location.href='Gender/index.php'" class=" btn-success btn-lg">Gender</button>
                    <button type="button" onclick="window.location.href='Hobby/index.php'" class=" btn-danger btn-lg">Hobby</button>
                    <button type="button" onclick="window.location.href='ProfilePicture/index.php'" class=" btn-primary btn-lg">Profile Picture</button>
                    <button type="button" onclick="window.location.href='SummaryOfOrganization/index.php'" class=" btn-success btn-lg">Summary Of Organization</button>




                </div>
            </td>

        </tr>
    </table>




</body>

<script>
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(1200).fadeOut();
</script>




<script>
    function ConfirmDelete(id)
    {
        var x = confirm("Are you sure you want to delete ID# "+id+" ?");
        if (x)
            return true;
        else
            return false;
    }




    $('#multiple_delete').on('click',function(){
        document.forms[0].action="deletemultiple.php";
        $('#multiple').submit();
    });

    $(document).ready(function() {
        $("#checkall").click(function() {
            var checkBoxes = $("input[name=mark\\[\\]]");
            checkBoxes.prop("checked", !checkBoxes.prop("checked"));
        });
    });





</script>


</HTML>

